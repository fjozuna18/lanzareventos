
const nombre = document.getElementById('nombre');
const saludar = document.getElementById('saludar');
const display = document.getElementById('display');

document.addEventListener('DOMContentLoaded', (e) => {
    saludar.addEventListener('click', saludarHandle);
    nombre.addEventListener('keypress', enterHandle);
});

const saludarHandle = (e) => {
    if (nombre.value) {
        display.innerHTML = 'Hola ' + nombre.value;
    } else {
        display.innerHTML = 'Hola mundo!';
    }
}

const enterHandle = (e) => {
    if (e.key === 'Enter') {
        const event = new Event('click');
        saludar.dispatchEvent(event);
    }
}